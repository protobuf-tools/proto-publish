package org.silbertb.gradle.protopublish

import org.gradle.api.Project

class ProtoPublishExtension {

    String previousVersion

    private Project project

    ProtoPublishExtension(Project project) {
        this.project = project
    }

    void useValidation() {
        project.configurations.protoPublish.dependencies.add(project.dependencies.create('io.envoyproxy.protoc-gen-validate:pgv-java-stub:0.6.1'))
    }
}
