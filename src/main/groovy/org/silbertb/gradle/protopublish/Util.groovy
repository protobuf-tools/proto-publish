package org.silbertb.gradle.protopublish

import org.gradle.api.Project

class Util {
    public static String ARTIFACT_FILE_NAME = "proto.zip"

    static String getBuildBaseDir(Project project) {
        return "$project.buildDir/protoPublish"
    }

    static String getArtifactDirName(Project project) {
        return getBuildBaseDir(project) + "/artifact"
    }

    static String getArtifactFileName(Project project) {
        return getArtifactDirName(project) + "/" + ARTIFACT_FILE_NAME
    }

    static String getSrcProtoDir(Project project) {
        return "$project.projectDir/src/main/proto"
    }

    static String getDependenciesDir(Project project) {
        return getBuildBaseDir(project) + "/deps"
    }

    static String getPrevVersionDir(Project project) {
        return getBuildBaseDir(project) + "/prev_version"
    }

    static String getCheckBreakingDepsDir(Project project) {
        return getBuildBaseDir(project) + "/check_breaking_deps"
    }

    static String getCheckLintWorkspaceDir(Project project) {
        return getBuildBaseDir(project) + "/check_lint_workspace"
    }

    static String getCheckBreakingWorkspaceDir(Project project) {
        return getBuildBaseDir(project) + "/check_breaking_workspace"
    }

    static String getExtractPrevProjectDir(Project project) {
        return getBuildBaseDir(project) + "/extract_prev_project_$project.name"
    }
}
