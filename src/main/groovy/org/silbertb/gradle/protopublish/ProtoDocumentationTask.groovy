package org.silbertb.gradle.protopublish

import groovy.io.FileType
import org.apache.tools.ant.taskdefs.condition.Os
import org.gradle.api.tasks.options.Option

class ProtoDocumentationTask extends BufTask {

    private ArrayList<String> generateDeps = []

    ProtoDocumentationTask() {
        super()
        dependsOn 'extractProtoDeps'
        dependsOn "assemble"

        workspace = Util.getCheckLintWorkspaceDir(project)
        if (!Os.isFamily(Os.FAMILY_UNIX)) {
            addDockerArgs(["--volume", Util.getBuildBaseDir(project) + "/doc:/doc"])
            addDockerArgs(["--volume", getProtocGenDocPath() + ":/protoc-gen-doc"])
        }

        def bufArgs = ["protoc",
                       "-Isrc", "-Ideps",
                       "--plugin=protoc-gen-doc=../protoc-gen-doc",
                        "--doc_out=../doc"]
        def docFile = project.file(Util.getBuildBaseDir(project) + "/doc")
        docFile.mkdirs()
        def dir = new File(Util.getSrcProtoDir(project))
        if(dir.exists()) {
            dir.eachFileRecurse(FileType.FILES) { file ->
                bufArgs.add("src/" + dir.relativePath(file))
            }
            addBufArgs(bufArgs)
        } else {
            project.logger.warn("missing dir: {}", dir.getPath())
            shouldExec = {false}
        }
    }

    @Option(option = "generateDeps", description = "generateDeps")
    void addGenerateDeps(List<String> generateDeps) {
        this.generateDeps.addAll(generateDeps)
    }

    @Override
    void run() {
        extractProtocGenDoc()
        addGenerateDepsToArgs()
        super.run()
    }

    private void addGenerateDepsToArgs() {
        File depsDir = new File(Util.getDependenciesDir(project))
        for(String generateDep : generateDeps) {
            File generateDepFile = new File(depsDir, generateDep)
            if(!generateDepFile.exists()) {
                project.logger.error("missing dep: {}", generateDepFile)
                shouldExec = {false}
                return;
            }

            generateDepFile.eachFileRecurse(FileType.FILES) { file ->
                addBufArgs(["deps/" + depsDir.relativePath(file)])
            }
        }
    }

    private void extractProtocGenDoc() {
        File targetFile = new File(getProtocGenDocPath())
        if (!targetFile.exists()) {
            targetFile.bytes = getClass().getClassLoader().getResourceAsStream('protoc-gen-doc').bytes
            targetFile.setExecutable(true)
        }
    }

    private String getProtocGenDocPath() {
        return Util.getBuildBaseDir(project) + "/" + "protoc-gen-doc"
    }
}
