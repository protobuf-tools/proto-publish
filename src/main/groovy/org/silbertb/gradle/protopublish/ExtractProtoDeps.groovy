package org.silbertb.gradle.protopublish


import org.gradle.api.DefaultTask
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

class ExtractProtoDeps extends DefaultTask {

    public ExtractProtoDeps()
    {
        setGroup( "Proto Publish" )
    }

    @OutputDirectory
    File getOutputDir()
    {
        return new File(Util.getDependenciesDir(project));
    }

    @TaskAction
    void run()
    {
        project.configurations.protoResolve.resolvedConfiguration.resolvedArtifacts.collect {
            def source = it.file
            def target = getOutputDir()
            logger.info("Copy {} to {}", source, target)
            project.copy {
                it.from project.zipTree(source)
                it.includeEmptyDirs = false
                it.include '**/*.proto'
                it.into target
            }

        }
    }

}
