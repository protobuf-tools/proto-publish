package org.silbertb.gradle.protopublish

import groovy.io.FileType
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.attributes.Usage
import org.gradle.api.component.SoftwareComponentFactory
import org.gradle.api.tasks.bundling.Zip

import javax.inject.Inject

class ProtoPublishPlugin implements Plugin<Project>  {

    private final SoftwareComponentFactory softwareComponentFactory

    private static String ARTIFACT_FILE_NAME = "proto.zip"

    @Inject
    ProtoPublishPlugin(SoftwareComponentFactory softwareComponentFactory) {
        this.softwareComponentFactory = softwareComponentFactory
    }

    @Override
    void apply(Project project) {
        project.plugins.apply('base')

        addProtoComponent(project)

        def ext = project.extensions.create('protoPublish', ProtoPublishExtension, project)

        defineArtifactToPublish(project)

        addPluginTasks(project, ext)
    }

    private void addProtoComponent(Project project) {

        //Define new configurations
        // 'protoResolve' used to resolve protobuf dependencies
        // 'protoPublish' used to publish protobuf as dependencies of the artifact
        // 'protoResolve' extends 'protoPublish' which means that using protoPublish will also resolve the dependency
        Configuration protoResolveConfiguration = project.configurations.create("protoResolve")
        Configuration protoPublishConfiguration = project.configurations.create("protoPublish")
        protoPublishConfiguration.setCanBeResolved(false)
        protoPublishConfiguration.setCanBeConsumed(true)

        protoResolveConfiguration.extendsFrom(protoPublishConfiguration)

        //add the component this means that it can be used in maven publish tasks
        def protoComponent = softwareComponentFactory.adhoc("proto")
        project.components.add(protoComponent)
        protoComponent.addVariantsFromConfiguration(protoPublishConfiguration,{
            it.mapToMavenScope("compile")
            it.mapToOptional()
        })

        protoPublishConfiguration.attributes( {
            it.attribute(Usage.USAGE_ATTRIBUTE, project.getObjects().named(Usage.class, "proto"));
        })

    }

    private void defineArtifactToPublish(Project project) {
        def openapiFile = project.file(Util.getArtifactFileName(project))
        project.artifacts.add('protoPublish', openapiFile) {
            it.setType('zip')
            it.setExtension('zip')
        }
    }

    void addPluginTasks(Project project, ProtoPublishExtension ext) {
        project.task("extractProtoDeps", type: ExtractProtoDeps) {
            setGroup( "Proto Publish")
        }

        ExtractPrevVersionTasks.addTasks(project, ext)

        project.task("checkBreaking", type: BufTask) {
            setGroup("Proto Publish")
            dependsOn 'extractProtoDeps'
            dependsOn 'extractPrevVersion'
            dependsOn "assemble"

            workspace = Util.getCheckBreakingWorkspaceDir(project)
            shouldExec = {ext.previousVersion != null}
            doFirst {
                if(!shouldExec.call()) {
                    return
                }
                project.copy {
                    from Util.getPrevVersionDir(project)
                    into Util.getCheckBreakingWorkspaceDir(project) + "/prev"
                }
            }

            addBufArgs(['breaking', '--against', 'prev'])
        }

        project.task("checkLint", type: BufTask) {
            setGroup( "Proto Publish" )
            dependsOn "extractProtoDeps"
            dependsOn "assemble"

            workspace = Util.getCheckLintWorkspaceDir(project)

            def bufArgs = ["lint"]
            def dir = new File(Util.getSrcProtoDir(project))
            if(dir.exists()) {
                dir.eachFileRecurse(FileType.FILES) { file ->
                    bufArgs.add("--path")
                    bufArgs.add("src/" + dir.relativePath(file))
                }
                addBufArgs(bufArgs)
            } else {
                project.logger.warn("missing dir: {}", dir.getPath())
                shouldExec = {false}
            }
        }

        project.task("assembleProto", type: Zip) {
            setGroup( "Proto Publish" )
            setDescription("Assemble archive of the protobuf files.");

            archiveFileName = ARTIFACT_FILE_NAME
            destinationDirectory = project.file(Util.getArtifactDirName(project))

            from(Util.getSrcProtoDir(project))
        }

        project.tasks.assemble.dependsOn "assembleProto"
        project.tasks.check.dependsOn "checkLint"
        project.tasks.check.dependsOn "checkBreaking"

        project.afterEvaluate {
            if(project.tasks.generateMetadataFileForMavenPublication) {
                project.tasks.generateMetadataFileForMavenPublication.dependsOn "build"
            }
        }


        handleDependenciesInSubprojects(project)
    }

    void handleDependenciesInSubprojects(Project project) {
        project.afterEvaluate {
            project.configurations.protoPublish.dependencies.forEach {dep ->
                handleDependencyInSubProject(dep, project)
            }

            project.configurations.protoResolve.dependencies.forEach {dep ->
                handleDependencyInSubProject(dep, project)
            }
        }
    }

    private void handleDependencyInSubProject(Dependency dep, project) {
        if (dep instanceof ProjectDependency) {
            dep.dependencyProject.afterEvaluate {
                if (dep.dependencyProject.tasks.findByName("assemble")) {
                    project.tasks.extractProtoDeps.dependsOn(dep.dependencyProject.assemble)
                }
            }
        }
    }
}
