package org.silbertb.gradle.protopublish


import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.WorkResult
import org.gradle.api.tasks.options.Option
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.apache.tools.ant.taskdefs.condition.Os

class BufTask extends DefaultTask {

    @Input
    String workspace = null

    @Input @Optional
    String bufYamlPath = null

    @Input @Optional
    Closure shouldExec;

    private List<String> bufArgs = new ArrayList<>()
    private List<String> dockerArgs = new ArrayList<>()

    @TaskAction
    void run() {
        if(shouldExec != null && !shouldExec.call()) {
            return
        }

        putBufYamlInWorkspace(project)
        prepareWorkspace(project)
        extractBufExecutable()
        List<String> args = prepareCommandAndArgs(project)
        def standardOutput = new ByteArrayOutputStream()
        def execResults = project.exec(new Action<ExecSpec>() {
            @Override
            void execute(ExecSpec execSpec) {
                if (Os.isFamily(Os.FAMILY_UNIX)) {
                    execSpec.setWorkingDir(workspace)
                }
                execSpec.setCommandLine(args)
                execSpec.standardOutput = standardOutput
                execSpec.setIgnoreExitValue(true)
            }
        } )

        handleFailure(execResults, standardOutput)
    }

    @Option(option = "bufArgs", description = "bufArgs")
    void addBufArgs(List<String> args) {
        bufArgs.addAll(args)
    }

    @Option(option = "dockerArgs", description = "dockerArgs")
    void addDockerArgs(List<String> args) {
        dockerArgs.addAll(args)
    }

    private List<String> prepareCommandAndArgs(Project project) {
        List<String> args
        if (Os.isFamily(Os.FAMILY_UNIX)) {
            args = [getBufExecPath()]
        } else {
            args = ['docker', "run",
                    "--volume", workspace + ":/workspace",
                    "--workdir", "/workspace",
                    "--rm"]
            args.addAll(dockerArgs)
            args.add("bufbuild/buf")
        }
        args.addAll(bufArgs)

        return args
    }

    protected void prepareWorkspace(Project project) {
        copySrcToWorkspace(project)
        copyDepsToWorkspace(project)
    }

    private void extractBufExecutable() {
        if (Os.isFamily(Os.FAMILY_UNIX)) {
            File targetFile = new File(getBufExecPath())
            if(!targetFile.exists()) {
                targetFile.bytes = getClass().getClassLoader().getResourceAsStream('buf-Linux-x86_64').bytes
                targetFile.setExecutable(true)
            }
        }
    }

    private String getBufExecPath() {
        return Util.getBuildBaseDir(project) + "/"+"buf"
    }

    private WorkResult copyDepsToWorkspace(Project project) {
        project.copy {
            from Util.getDependenciesDir(project)
            into "$workspace/deps"
        }
    }

    private WorkResult copySrcToWorkspace(Project project) {
        project.copy {
            from Util.getSrcProtoDir(project)
            into "$workspace/src"
        }
    }

    private void putBufYamlInWorkspace(Project project) {
        String bufYamlText;
        if(bufYamlPath == null) {
            bufYamlText = getClass().getClassLoader().getResourceAsStream('buf.yaml').text
        } else {
            bufYamlText = new File(bufYamlPath).text
        }

        //project.println("Buf.yaml text: " + bufYamlText)
        def bufYamlFile = project.file(workspace + '/buf.yaml')
        if (!bufYamlFile.isFile()) {
            bufYamlFile.parentFile.mkdirs()
            bufYamlFile.createNewFile()
        }
        bufYamlFile.write(bufYamlText)
    }

    private void handleFailure(ExecResult execResult, OutputStream standardOutput) {
        if (execResult.getExitValue() != 0) {
            if(execResult.getExitValue() == 1) {
                throw new GradleException(standardOutput.toString())
            } else {
                throw new GradleException('Non-zero exit value: ' + execResult.getExitValue() + " " + standardOutput.toString())
            }
        }
    }
}
