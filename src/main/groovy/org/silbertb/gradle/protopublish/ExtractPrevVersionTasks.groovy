package org.silbertb.gradle.protopublish

import org.gradle.api.Project
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.tasks.GradleBuild

/**
 * Add tasks for extracting a different version of that same project.
 * This is actually something hard to do. Gradle has a bug that prevent fro  doing it using gradle configuration (i.e. dependencies key words).
 * When doing it that way it tries to fetch the project artifact instead of looking in the defined repositories.
 * The solution is to add a different gradle project and call it using a 'GradleBuild' task.
 */
class ExtractPrevVersionTasks {
    static void addTasks(Project project, ProtoPublishExtension ext) {
        project.task("prepareExtractPrevProject") {
            setGroup("Proto Publish")

            String extractProjectDir = Util.getExtractPrevProjectDir(project)
            def buildGradleFile = project.file("$extractProjectDir/build.gradle")
            doLast {
                def buildGradleTemplate = getClass().getClassLoader().getResourceAsStream('extract_prev/build.gradle').text
                def buildGradleText = buildGradleTemplate.replace([
                        "__REPOS__": listMavenRepos(project),
                        "__DEP__": "$project.group:$project.name:$ext.previousVersion",
                        "__COMMENT_DEP__": ext.previousVersion == null ? "//" : ""])
                if (!buildGradleFile.isFile()) {
                    buildGradleFile.parentFile.mkdirs()
                    buildGradleFile.createNewFile()
                }
                buildGradleFile.write(buildGradleText)
                def settingGradleFile = project.file("$extractProjectDir/settings.gradle")
                settingGradleFile.write("rootProject.name = 'extract_prev_$project.name'")
            }
        }

        project.task('extractPrevVersion', type: GradleBuild) {
            setGroup("Proto Publish")
            dependsOn "prepareExtractPrevProject"

            def buildGradleFile = project.file(Util.getExtractPrevProjectDir(project) + "/build.gradle")

            dir = project.file(Util.getExtractPrevProjectDir(project))
            tasks = ['extractMyconf']
            project.logger.info("addExtractPrevVersionTask: done config")

        }
    }

    private static String listMavenRepos(Project project) {
        StringBuilder sb = new StringBuilder()
        project.repositories.each {repo ->
            if(repo instanceof MavenArtifactRepository) {
                sb.append "maven {\n"
                sb.append "url '$repo.url'\n"
                repo.artifactUrls.collect() { artUrl ->
                    sb.append "artifactUrls '$artUrl'\n"
                }
                sb.append "}\n"
            } else {
                project.logger.info("ignore repo: Name: {}", repo.name)
            }
        }
        return sb.toString()
    }
}

