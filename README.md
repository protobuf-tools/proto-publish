# Proto Publish
## About the Project
This gradle plugin can publish schemas to maven repositories, while doing related tasks and validation
* backward compatibility validations
* Conventions validation
* Generate api documentation

The other side of it, consuming the schemas, can be done using the [_Proto Consume_](https://gitlab.com/protobuf-tools/proto-consume) gradle plugin.

When using schema to represent the data model and api one of the most important thing is sharing it between projects. When the schema is used for code generation then it becomes important to do it when building the project. The idea is to share the schema and then generate the code rather than sharing a generated package.

This Gradle Plugin provides an alternative for using schema registry in compile time. 
* It uses maven repository to share the schemas.
* It uses [_Buf.Build_](https://docs.buf.build) to check conventions and backward compatibility.
* It uses [_protoc-gen-doc_](https://github.com/pseudomuto/protoc-gen-doc) with [_Buf.Build_](https://docs.buf.build) to generate documentation

## Why not schema registry?
One of the ways to do it is utilizing a schema registry. Schema registries provides more than just sharing - they can enforce conventions over the api and check for backword compatibility. 
There are, however, some drawbacks for using schema registries during the build pipeline:
* There is a need to maintain a server for this repository, which require resources, dependencies on other teams and adds complexity
* When working on local machine there is still a need to communicate with the server to check the conventions and backword compatibility
* Accessing the schema registry during build is not always trivial. They tend to be oriented for use in runtime, for integration in message queues like kafka or pulsar.

## Pre-requisite
For non-linux machines - a docker engine should be installed on the machine which carries the build.

## How to use it?
An example for usage can be found in [_Proto Service Example_](https://gitlab.com/protobuf-tools/proto-service-example) repo.  
Detailed explanation below.

### Apply the plugin
```groovy
plugins {
    id 'base'
    id 'maven-publish'
    id "com.gitlab.protobuf-tools.proto-publish" version "1.0.0"
}
```

### Publish to maven repository
Use the 'maven-publish' plugin, apply it as described above.
the relevant component is 'proto' so you can add a maven publish block like this:
```groovy
publishing {
    publications {
        maven(MavenPublication) {
            from components.proto
        }
    }
}
```
Of course, it is possible to use all the customization options maven publish provides - for repository address, security and so on.
Another important feature maven publish provides it the task "publishToMavenLocal" which enables using local machine without any server. This might be useful during the development process itself, before publishing to all consumers.

### Check Lint
In order to chek proper conventions of the schema definition use the task "checkLint". Currently, it checks [_Buf.Build_](https://docs.buf.build) default conventions (with some exception). In the future there will be an option for customization.

### Check Backward Compatibility
In order to check backward compatibility use the task 'checkBreaking'. Before doing it you must specify the api version to check against. 
To do it add a 'protoPublish' configuration block:
```groovy
protoPublish {
    previousVersion = "Your Version"
}
```
Currently, it checks [_Buf.Build_](https://docs.buf.build) 'WIRE_JSON' tests. In the future there will be an option for customization.
'WIRE_JSON' means that it protects against breaking api between processes but allow breaking usage of generated code.

### Add dependencies on other schemas
Sometimes the schema you write may depend on another schema. For example when writing a grpc service which uses an already defined data model. In order to allow it there is a need to declare this dependency.
Generally there are two kinds of dependencies:
* Transitive dependencies - declare it if you want users of your schema to get the dependency schema as well. When publishing your schema this dependency will be published as well.
* Non-transitive dependency - use the dependency only to make the different checks (linting, backward compatibility) possible, but don't publish it as dependency to the consumers.
Most of the time the Transitive Dependency is the relevant time.
Declaring the dependency is done in the 'dependencies' block like all dependencies in gradle, but there are two new keywords from protobuf schemas dependencies:
* __protoPublish__ - Transitive Dependency 
* __protoResolve__ - Non-Transitive Dependency

Example:
```groovy
dependencies {
    protoPublish 'org.silbertb.protobuf:example-data-model:1.0.0'
}
```
### Add schema validation definitions
When defining schema there should be a balance between two opposite goals:
* Clear requirements for the payload content and strict validation to avoid mistakes
* Forward compatibility and ease of evolving the schema
Protobuf has taken the Forward Compatibility side to the extent while neglecting, in my opinion, too much the need for clear requirement and strict validation. Fortunately, envoy has developed a very good protoc plugin to cover for that - [_protoc-gen-validate_](https://github.com/envoyproxy/protoc-gen-validate).
In order to encourage schema validation I have added some sugaring which makes declaring the dependency on this plugin as easy as possible.
Simply add "useValidation()" to the 'protoPublish' configuration block.
Example: 
```groovy
protoPublish {
    useValidation()
}
```

### Generate documentation
The plugin provides the gradle task type _ProtoDocumentationTask_. In order to generate documentation, define a task using this class type.
It generates documentation for all the proto files under _src/main/proto_. It is also possible generate documentation for selected dependencies. Just use _addGenerateDeps_ inside the task configuration. It accepts a list of directories/files and will generate documentation for all the proto files under it recursively.

Example:
```groovy
import org.silbertb.gradle.protopublish.ProtoDocumentationTask

task protoDoc(type: ProtoDocumentationTask) {
    addGenerateDeps(["google"])
}
```

### Other gradle tasks
* extractProtoDeps - extract all the declared protobuf dependencies int the folder "build/protoPublish/deps"
* extractPrevVersion - extract the declared previous version for backward compatibility check into the folder "build/protoPublish/prev_version"

## Roadmap

Please use [open issues](https://gitlab.com/protobuf-tools/proto_domain_converter/-/issues) to propose features and report defects.

## Contributing

Any contributions are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

## License

Distributed under the APACHE License. See [LICENSE](./LICENSE) for more information.

## Contact

Barak Silbert - silbert.barak@gmail.com 

Project Link: <https://gitlab.com/protobuf-tools/proto-publish>


